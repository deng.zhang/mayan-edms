# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Wojciech Warczakowski <w.warczakowski@gmail.com>, 2016
# Wojciech Warczakowski <w.warczakowski@gmail.com>, 2017
msgid ""
msgstr ""
"Project-Id-Version: Mayan EDMS\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-06-07 21:03-0400\n"
"PO-Revision-Date: 2018-04-10 08:24+0000\n"
"Last-Translator: Roberto Rosario\n"
"Language-Team: Polish (http://www.transifex.com/rosarior/mayan-edms/language/pl/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: pl\n"
"Plural-Forms: nplurals=4; plural=(n==1 ? 0 : (n%10>=2 && n%10<=4) && (n%100<12 || n%100>14) ? 1 : n!=1 && (n%10>=0 && n%10<=1) || (n%10>=5 && n%10<=9) || (n%100>=12 && n%100<=14) ? 2 : 3);\n"

#: apps.py:41 events.py:7 links.py:31
msgid "Checkouts"
msgstr "Blokady"

#: dashboard_widgets.py:18
msgid "Checkedout documents"
msgstr "Dokumenty zablokowane"

#: events.py:11
msgid "Document automatically checked in"
msgstr "Dokument został automatycznie odblokowany"

#: events.py:14
msgid "Document checked in"
msgstr "Dokument został odblokowany"

#: events.py:17
msgid "Document checked out"
msgstr "Dokument został zablokowany"

#: events.py:21
msgid "Document forcefully checked in"
msgstr "Wymuszono odblokowanie dokumentu"

#: exceptions.py:27 views.py:49
msgid "Document already checked out."
msgstr "Dokument jest już zablokowany."

#: forms.py:28
msgid "Document status"
msgstr "Status dokumentu"

#: forms.py:37 models.py:41 views.py:84
msgid "User"
msgstr "Użytkownik"

#: forms.py:41
msgid "Check out time"
msgstr "Czas blokady"

#: forms.py:46
msgid "Check out expiration"
msgstr "Wygaśnięcie blokady"

#: forms.py:51
msgid "New versions allowed?"
msgstr "Czy nowe wersje są dozwolone?"

#: forms.py:52
msgid "Yes"
msgstr "Tak"

#: forms.py:52
msgid "No"
msgstr "Nie"

#: links.py:37
msgid "Check out document"
msgstr "Zablokuj dokument"

#: links.py:42
msgid "Check in document"
msgstr "Odblokuj dokument"

#: links.py:49
msgid "Check in/out"
msgstr "Blokada"

#: literals.py:12
msgid "Checked out"
msgstr "Zablokowany"

#: literals.py:13
msgid "Checked in/available"
msgstr "Odblokowany/dostępny"

#: models.py:28 models.py:99
msgid "Document"
msgstr "Dokument"

#: models.py:31
msgid "Check out date and time"
msgstr "Data i czas blokady"

#: models.py:35
msgid "Amount of time to hold the document checked out in minutes."
msgstr "Liczba dni, godzin lub minut w trakcie których dokument będzie zablokowany."

#: models.py:37
msgid "Check out expiration date and time"
msgstr "Data i czas wygaśnięcia blokady"

#: models.py:46
msgid "Do not allow new version of this document to be uploaded."
msgstr "Brak możliwości dodania nowej wersji dokumentu."

#: models.py:48
msgid "Block new version upload"
msgstr "Blokuj załadowanie nowej wersji"

#: models.py:55 permissions.py:7
msgid "Document checkout"
msgstr "Blokada dokumentu"

#: models.py:56
msgid "Document checkouts"
msgstr "Blokady dokumentu"

#: models.py:64
msgid "Check out expiration date and time must be in the future."
msgstr "Wygaśnięcie blokady musi nastąpić w przyszłości."

#: models.py:105
msgid "New version block"
msgstr "Blokada nowej wersji"

#: models.py:106
msgid "New version blocks"
msgstr "Blokady nowych wersji"

#: permissions.py:10
msgid "Check in documents"
msgstr "Odblokuj dokumenty"

#: permissions.py:13
msgid "Forcefully check in documents"
msgstr "Wymuś odblokowanie dokumentów"

#: permissions.py:16
msgid "Check out documents"
msgstr "Zablokuj dokumenty"

#: permissions.py:19
msgid "Check out details view"
msgstr "Podgląd szczegółów blokady"

#: queues.py:8
msgid "Checkouts periodic"
msgstr ""

#: queues.py:12
msgid "Check expired checkouts"
msgstr ""

#: serializers.py:26
msgid "Primary key of the document to be checked out."
msgstr ""

#: views.py:53
#, python-format
msgid "Error trying to check out document; %s"
msgstr "Błąd podczas blokady dokumentu: %s"

#: views.py:58
#, python-format
msgid "Document \"%s\" checked out successfully."
msgstr "Dokument \"%s\" został pomyślnie zablokowany."

#: views.py:66
#, python-format
msgid "Check out document: %s"
msgstr "Zablokuj dokument: %s"

#: views.py:81
msgid "Documents checked out"
msgstr "Dokumenty zablokowane"

#: views.py:90
msgid "Checkout time and date"
msgstr "Rozpoczęcie blokady"

#: views.py:96
msgid "Checkout expiration"
msgstr "Wygaśnięcie blokady"

#: views.py:116
#, python-format
msgid "Check out details for document: %s"
msgstr "Szczegóły blokady dokumentu: %s"

#: views.py:134
#, python-format
msgid ""
"You didn't originally checked out this document. Forcefully check in the "
"document: %s?"
msgstr "Ten dokument nie został przez ciebie zablokowany. Czy wymusić odblokowanie dokumentu: %s?"

#: views.py:138
#, python-format
msgid "Check in the document: %s?"
msgstr "Odblokować dokument: %s?"

#: views.py:166
msgid "Document has not been checked out."
msgstr "Dokument nie został zablokowany."

#: views.py:171
#, python-format
msgid "Error trying to check in document; %s"
msgstr "Błąd podczas odblokowania dokumentu: %s"

#: views.py:176
#, python-format
msgid "Document \"%s\" checked in successfully."
msgstr "Dokument \"%s\" został pomyślnie odblokowany."

#: widgets.py:22
msgid "Period"
msgstr "Okres"
